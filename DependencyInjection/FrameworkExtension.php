<?php

namespace HotWire\Framework\DependencyInjection;

use HotWire\DependencyInjection\Extension;
use HotWire\Framework\Config\Parameters;
use HotWire\Framework\Handler\SessionHandler;

class FrameworkExtension extends Extension
{
    public function load()
    {
        $this->container->register('parameters', Parameters::getInstance())
                        ->register('session.handler',new SessionHandler());

        return $this;
    }
}
