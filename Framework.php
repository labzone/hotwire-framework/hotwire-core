<?php

namespace HotWire\Framework;

use HotWire\Routing\Matcher;
use HotWire\Framework\Resolver\ControllerResolver;
use HotWire\Http\Request;
use HotWire\Http\Response;
use HotWire\EventDispatcher\Listener;
use HotWire\Framework\Controller\Controller;
use HotWire\Framework\Controller\ErrorController;
use HotWire\Routing\Exception\ResourceNotFoundException;

class Framework
{
    protected $dispatcher;
    protected $matcher;
    protected $resolver;

    public function __construct(Listener $dispatcher, Matcher $matcher, ControllerResolver $resolver)
    {
        $this->dispatcher=$dispatcher;
        $this->matcher=$matcher;
        $this->resolver=$resolver;
    }

    public function handle(Request $request)
    {
        try {
            $request->setAttributes($this->matcher->match($request->getPathInfo()));
            $controller=$this->resolver->getController($request);
            $arguments=$this->resolver->getArguments($request, $controller);
            if (is_array($controller)) {
                return self::getResponse(call_user_func_array($controller, $arguments));
            }
            throw new \Exception("Error Processing Request", 1);
        } catch (ResourceNotFoundException $e) {
            try {
                $request->setAttributes(array('controller'=>'HotWire\Framework\Controller\ErrorController:error404Action'));
                $controller=$this->resolver->getController($request);

                return self::getResponse(call_user_func_array($controller, array()));
            } catch (\Exception $e) {
                $request->setAttributes(array('controller'=>'HotWire\Framework\Controller\ErrorController:error500Action'));
                $controller=$this->resolver->getController($request);

                return self::getResponse(call_user_func_array($controller, array($e)));
            }
        }
    }

    private static function getResponse($content)
    {
        if ($content instanceof Response) {
            return $content;
        }

        return new Response($content);
    }
}
