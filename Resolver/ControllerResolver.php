<?php

namespace HotWire\Framework\Resolver;

use HotWire\Http\Request;
use HotWire\Routing\RouteCollection;
use HotWire\DependencyInjection\Container;

class ControllerResolver implements IResolver
{
    public static function getController(Request $request)
    {
        extract(self::seperateControllerMethod($request));
        $container=Container::getInstance();
        $controllerInstance=null;
        $reflectionClass=new \ReflectionClass($controller);
        if ($constructor=$reflectionClass->getConstructor()) {
            $parameters=array();
            foreach ($constructor->getParameters() as $key => $parameter) {
                if ($class=$parameter->getClass()) {
                    $parameters[]=$container->create($class->getName());
                }
            }
            $controllerInstance=$reflectionClass->newInstanceArgs($parameters);
        } else {
            $controllerInstance=$reflectionClass->newInstance();
        }
        $controllerInstance->setContainer($container);

        return array(
            $controllerInstance,
            $method
        );
    }

    public static function getArguments(Request $request, $controller)
    {
        $arguments=array();
        extract(self::seperateControllerMethod($request));
        $reflectionClass=new \ReflectionClass($controller);
        $reflectionMethod=$reflectionClass->getMethod($method);
        $parameters=$reflectionMethod->getParameters();
        foreach ($parameters as $parameter) {
            if ($c=$parameter->getClass()) {
                if ($c->getName()=='HotWire\Http\Request') {
                    $arguments[$parameter->getName()]=$request;
                }
            } else {
                $route=RouteCollection::getRoutes()[$request->getAttributes()['route']];
                $arguments[$parameter->getName()]=$request->getAttributes()['parameters'][$parameter->getName()];
            }
        }

        return $arguments;
    }

    public static function seperateControllerMethod(Request $request)
    {
        $items=array('controller','method');
        $parts=explode(':', $request->getAttributes()['controller']);

        return array_combine($items,$parts);
    }
}
