<?php

namespace HotWire\Framework\App;

use HotWire\Routing\RouteCollection;
use HotWire\Framework\App as Framework;
use HotWire\DependencyInjection\Container;

abstract class AbstractKernel
{
    private $routeCollection;

    protected static $instance;

    private function __construct()
    {
        Container::getInstance()->register('kernel', $this);
        $this->routeCollection=new RouteCollection();
        $this->boot();
    }

    private function __clone() { }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance=new Static();
        }

        return self::$instance;
    }

    private function addRoutes($routing)
    {
        if (class_exists($routing,true)) {
            $reflectionClass = new \ReflectionClass($routing);
            if ($reflectionClass->implementsInterface('HotWire\Routing\IRoute')) {
                foreach ($routing::getRoutes() as $name => $route) {
                    $this->routeCollection->add($name,$route);
                }
            }
        }
    }

    private function boot()
    {
        $framework=null;
        foreach ($this->getApps() as $app) {
            $this->addRoutes(str_replace('App','Routing\\Routing',get_class($app)));
            $app->preBoot();
            $app->boot();
            $app->postBoot();
            if ($app instanceof Framework) {
                $framework=$app;
            }
        }
        $framework->handleRequest();
    }

    abstract public function getApps();
}
