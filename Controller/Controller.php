<?php

namespace HotWire\Framework\Controller;

use HotWire\DependencyInjection\Container;

abstract class Controller
{
    protected $container;
    protected $templateType;

    /**
     * set container
     * @param Container $container
     */
    public function setContainer(Container $container)
    {
        $this->container=$container;

        return $this;
    }

    /**
     * get entity manager
     * @return HotWire\ORM\Manager
     */
    public function getEntityManager()
    {
        return $this->get('entity.manager');
    }

    /**
     * get service by its name
     * @param  string $serviceName
     * @return Object service
     */
    public function get($serviceName)
    {
        return $this->container->get($serviceName);
    }

    /**
     * render view
     * @param  string                $name       view name
     * @param  array                 $parameters parameters to pass to view
     * @return HotWire\Http\Response
     */
    public function render($name, $parameters = array())
    {
        $templateType='templating'.($this->templateType ? ".{$this->templateType}":null);

        return $this->get($templateType)
                    ->setView($name)
                    ->setParameters($parameters)
                    ->render();
    }

    public function redirect($path)
    {
        header("Location: /index.php/{$path}");
        exit;
    }
}
