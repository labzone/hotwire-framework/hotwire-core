<?php

namespace HotWire\Framework\Controller;

/**
 * Controller to handle 404 and 500 errors
 */
class ErrorController extends Controller
{
    /**
     * explicitly set templating engine
     * @var string
     */
    protected $templateType = 'twig';

    /**
     * handle exception 404
     * @return HotWire\Http\Response
     */
    public function error404Action()
    {
        return $this->render('HotWire:Framework/Error/404');
    }

    /**
     * handle exception 500
     * @return HotWire\Http\Response
     */
    public function error500Action(\Exception $exception)
    {
        return $this->render('HotWire:Framework/Error/500',array(
            'exception'=>$exception
        ));
    }
}
