<?php

namespace HotWire\Framework;

use HotWire\DependencyInjection\Extension;
use HotWire\DependencyInjection\Factory\ExtensionFactory;
use HotWire\Routing\Matcher;
use HotWire\Framework\Resolver\ControllerResolver;
use HotWire\DependencyInjection\Container;
use HotWire\Http\Request;

abstract class AbstractApp
{
    /**
     * application path
     * @var string
     */
    private $path;

    /**
     * extension factory
     * @var ExtensionFactory
     */
    protected $extensionFactory;

    /**
     * get extension factory instance
     */
    public function __construct()
    {
        $this->extensionFactory=ExtensionFactory::getInstance();
    }

    /**
     * get app path
     * @return string
     */
    public function getAppPath()
    {
        return realpath(self::getRootPath().'/src/'.trim(str_replace('\\','/',get_called_class()),'App'));
    }

    /**
     * get root path
     * @return string
     */
    public static function getRootPath()
    {
        $dir=__DIR__;
        if (strstr($dir,'vendor')) {
            $delimiter='vendor';
        } else {
            $delimiter='src';
        }
        $items=explode($delimiter, $dir);

        return current($items);
    }

    /**
     * get cache path
     * @return string
     */
    public static function getCachePath()
    {
        return self::getRootPath().'app/cache/';
    }

    /**
     * get config path
     * @return string
     */
    public static function getConfigPath()
    {
        return self::getRootPath().'app/config/';
    }

    /**
     * get resource path
     * @return string
     */
    public static function getResourcePath()
    {
        return self::getRootPath().'app/Resources/';
    }

    /**
     * get view path
     * @return string|null
     */
    public function getViewsPath()
    {
        $dir=self::getRootPath().'src/'.str_replace(':', '\\', $this->getName()).'/Resources/Views/';
        if (file_exists($dir) && is_dir($dir)) {
            return $dir;
        }

        return null;
    }

    /**
     * do operation before boot
     */
    public function preBoot() { }

    /**
     * do operation when running module once
     */
    public function boot()
    {
        $this->preBoot();
        $this->registerExtension();
        $this->postBoot();
    }

    /**
     * do operation after boot
     */
    public function postBoot() { }

    /**
     * register extension
     */
    private function registerExtension()
    {
        $extension=$this->getExtension();
        if ($extension instanceof Extension) {
            $extension->load();
        }
    }

    /**
     * get extension instance
     * @return HotWire\DepedencyInjection\Extension
     */
    private function getExtension()
    {
        return $this->extensionFactory->create($this->getName());
    }

    /**
     * handle incoming request
     * @return self
     */
    public function handleRequest()
    {
        $eventDispatcher=Container::getInstance()->get('event.dispatcher');
        $matcher=new Matcher();
        $resolver=new ControllerResolver();
        $request=new Request();
        $framework=new Framework($eventDispatcher, $matcher, $resolver);
        $response=$framework->handle($request);
        $response->send();

        return $this;
    }

    /**
     * get app name
     * @return string
     */
    abstract public function getName();
}
