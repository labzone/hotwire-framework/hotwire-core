<?php

namespace HotWire\Framework\Config;

use HotWire\ORM\Config\Database;
use HotWire\Framework\App;
use HotWire\Util\Collection\ArrayList;

class Parameters
{
    private $templating;
    private $database;
    private $debug;
    private $smtp;

    private static $instance;

    private function __construct()
    {
        if (file_exists(self::getParametersFile())) {
            $parameters=json_decode(file_get_contents(self::getParametersFile()), true);

            $this->mapParameters($parameters);
        }
    }

    private function __clone() { }

    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance=new Self();
        }

        return self::$instance;
    }

    private function mapParameters(array $parameters)
    {
        foreach ($parameters as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key=$value;
            }
        }

        return $this;
    }

    public static function getParametersFile()
    {
        return App::getConfigPath().'parameters.json';
    }

    public function isDebug()
    {
        return $this->debug;
    }

    public function getSmtpParameters()
    {
        return $this->smtp;
    }

    public function getTemplating()
    {
        return $this->templating;
    }

    public function getDatabaseConfig()
    {
        return new ArrayList($this->database);
    }
}
